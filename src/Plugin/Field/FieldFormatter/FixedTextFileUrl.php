<?php

namespace Drupal\fixed_text_link_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'file_url_plain' formatter.
 *
 * @FieldFormatter(
 *   id = "fixed_text_file_url",
 *   label = @Translation("Link with a fixed text"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class FixedTextFileUrl extends FileFormatterBase {

  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    protected readonly FileUrlGeneratorInterface $fileUrlGenerator,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('file_url_generator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'link_text' => 'Download',
      'link_class' => '',
      'open_in_new_window' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $parentForm, FormStateInterface $form_state) {
    $parentForm = parent::settingsForm($parentForm, $form_state);

    $form['link_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text'),
      '#default_value' => $this->getLinkText(),
      '#required' => TRUE,
    ];

    $form['link_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link class'),
      '#default_value' => $this->getLinkClass(),
      '#required' => FALSE,
    ];

    $form['open_in_new_window'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open in a new window'),
      '#default_value' => $this->openLinkInNewWindow(),
    ];

    return $form + $parentForm;
  }

  /**
   * @return string
   */
  private function getLinkText() {
    return $this->getSettings()['link_text'];
  }

  /**
   * @return string
   */
  private function getLinkClass() {
    return $this->getSettings()['link_class'];
  }

  /**
   * @return bool
   */
  private function openLinkInNewWindow() {
    return $this->getSettings()['open_in_new_window'];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Link text: @text', ['@text' => $this->getLinkText()]);
    if (!empty($settings['link_class'])) {
      $summary[] = $this->t('Link class: @text', ['@text' => $this->getLinkClass()]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /** @var \Drupal\file\Entity\File $file */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $text = $this->getLinkText();
      $url = $this->fileUrlGenerator->generate($file->getFileUri());
      $link = Link::fromTextAndUrl($text, $url);

      $build = $link->toRenderable();

      if (!empty($this->getLinkClass())) {
        $build['#attributes']['class'][] = $this->getLinkClass();
      }

      if ($this->openLinkInNewWindow()) {
        $build['#attributes']['target'] = '_blank';
      }
      $cacheableMetadata = CacheableMetadata::createFromObject($file);
      $cacheableMetadata->applyTo($build);
      $elements[$delta] = $build;
    }

    return $elements;
  }

}
